//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ProjCleaner.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PROJCLEANER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_CONFIG_DLG                  129
#define IDR_PATH_MENU                   130
#define IDB_FOLDER_BMP                  131
#define IDR_TREE_MENU                   132
#define RES_XML_DEFAULT                 133
#define IDB_FILE_BMP                    135
#define IDR_LOG_MENU                    136
#define IDR_TRAY_MENU                   137
#define IDB_BITMAP1                     138
#define IDB_TOP_BMP                     138
#define IDD_DUTY_DLG                    139
#define IDB_FOLDER_CLOSE                143
#define IDB_BITMAP3                     144
#define IDB_FOLDER_OPEN                 144
#define IDC_ADD_PATH                    1001
#define IDC_SET_INFO                    1002
#define IDC_LOG_LIST                    1003
#define IDC_SCAN_DEL                    1004
#define IDC_PROGRESS                    1005
#define IDC_PATH_TREE                   1006
#define IDC_RES_DEFAULT                 1007
#define IDC_DEL_ALL                     1008
#define IDC_TREE_SELECT                 1009
#define IDC_SAVE_LIST                   1010
#define IDC_LOAD_LIST                   1012
#define IDC_SET_DEFAULT                 1015
#define IDC_DEL_RECYCLE_CHECK           1016
#define IDC_END_CLOSE_CHECK             1017
#define IDC_PATH_LIST                   1018
#define IDC_DEL_PATH                    1019
#define IDC_LOG_BTN                     1020
#define IDC_APPLY_BTN                   1022
#define IDC_SAVE_PATH_LIST              1024
#define IDC_MIN_TRAY_CHECK              1025
#define IDC_SMING_BTN                   1026
#define IDC_MINZE_BTN                   1027
#define IDC_EDIT_DUTY                   1029
#define IDC_STATIC_PICTURE              1030
#define IDC_CHECK1                      1031
#define IDC_ASS_R_MENU_CHECK            1031
#define ID_MENU_32771                   32771
#define ID_MENU_32772                   32772
#define ID_MENU_32773                   32773
#define ID_MENU_ADD_PATH                32774
#define ID_M_DEL_PATH                   32775
#define ID_M_DEL_PATHS                  32776
#define ID_M_ADD_PATH                   32777
#define ID_MENU_32778                   32778
#define ID_MENU_32779                   32779
#define ID_MENU_32780                   32780
#define ID_MENU_32781                   32781
#define ID_MENU_32782                   32782
#define ID_MENU_32783                   32783
#define ID_CM_ADD_FOLDER                32784
#define ID_CM_ADD_EXT                   32785
#define ID_CM_DEL_CUR                   32786
#define ID_CM_DEL_ITEM                  32787
#define ID_CM_DEL_ITEMS                 32788
#define ID_CM_RENAME                    32789
#define ID_MENU_32790                   32790
#define ID_MENU_32791                   32791
#define ID_MENU_32792                   32792
#define ID_MENU_32793                   32793
#define ID_M_DEL_LOGS                   32794
#define ID_M_DEL_OK_LOGS                32795
#define ID_M_DEL_ERROR_LOGS             32796
#define ID_M_SAVE_LOGS                  32797
#define ID_M_DEL_ALL_LOGS               32798
#define ID_MENU_32799                   32799
#define ID_MENU_32800                   32800
#define ID_M_SHOW                       32801
#define ID_M_EXIT                       32802
#define ID_Menu                         32803
#define ID_M_OPEN_AT_FOLDER             32804

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32805
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
